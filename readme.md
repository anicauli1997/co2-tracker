## CO2 Tracker

### This mini-project contains three models:
1. User
2. Sensor
3. SensorTrack

The User model contains records about the user that can manage the system (administrators).
To have access to the platform they must login and use the access token.
The users can be able to read and create sensors.

Sensor model contains records for the installed sensors in the districts of the cities.
This model is composed by city, district, and a token that is generated at the moment when it's created.
This should be secret. With this token, you can be able to read and create
sensor tracks (SensorTrack), which is the model that contains records for the 
tracks that each sensor collects.

To be able to read or create tracks, no User authentication is needed, but you should use the sensor token.
It is made like so, because it is supposed that the sensors will be configured in the districts of the city, 
and they can get the token as their configuration to interact with their data.

To run the application, it needed to have two databases in your localhost.
One named "co_track" by user "root" and no password, and the other named "test_co_track" that is user for tests.

By starting the spring boot application (port 8080 by its default), some default data will be seeded (Check the command classes).

To access the API Endpoints, I have added swagger, so after running the app navigate to: http://localhost:8080/swagger-ui/index.html#/

Each API is covered by test.