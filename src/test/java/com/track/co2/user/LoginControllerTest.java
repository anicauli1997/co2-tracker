package com.track.co2.user;

import com.track.co2.model.user.User;
import com.track.co2.repository.user.UserRepository;
import com.track.co2.request.user.LoginRequestDto;
import com.track.co2.response.user.LoginResponseDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
public class LoginControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setup() {
        userRepository.deleteAll();
        User user = new User();
        user.setUsername("user");
        user.setPassword(passwordEncoder.encode("password"));
        userRepository.save(user);
    }

    @Test
    void testLogin() {
        // Test the login
        String url = "http://localhost:" + port + "/api/login";
        LoginRequestDto loginRequest = new LoginRequestDto();
        loginRequest.setUsername("user");
        loginRequest.setPassword("password");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LoginRequestDto> entity = new HttpEntity<>(loginRequest, headers);
        ResponseEntity<LoginResponseDto> response = restTemplate.exchange(
                url, HttpMethod.POST, entity, LoginResponseDto.class
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void testLoginWrongCredentials() {
        // Test a wrong login by providing wrong credentia;s
        String url = "http://localhost:" + port + "/api/login";
        LoginRequestDto loginRequest = new LoginRequestDto();
        loginRequest.setUsername("user");
        loginRequest.setPassword("wrong_password");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LoginRequestDto> entity = new HttpEntity<>(loginRequest, headers);
        ResponseEntity<LoginResponseDto> response = restTemplate.exchange(
                url, HttpMethod.POST, entity, LoginResponseDto.class
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
}
