package com.track.co2.sensor;

import com.track.co2.model.sensor.Sensor;
import com.track.co2.model.user.User;
import com.track.co2.repository.sensor.SensorRepository;
import com.track.co2.repository.user.UserRepository;
import com.track.co2.request.sensor.SensorCreateRequestDto;
import com.track.co2.request.user.LoginRequestDto;
import com.track.co2.response.PageResponse;
import com.track.co2.response.sensor.SensorResponseDto;
import com.track.co2.response.user.LoginResponseDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Objects;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
public class SensorControllerTest {
    @LocalServerPort
    private int port;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SensorRepository sensorRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setup() {
        userRepository.deleteAll();
        User user = new User();
        user.setUsername("user");
        user.setPassword(passwordEncoder.encode("password"));
        userRepository.save(user);

        sensorRepository.deleteAll();

        Sensor sensorParallel = new Sensor();
        sensorParallel.setCity("Barcelona");
        sensorParallel.setDistrict("Parallel");
        sensorParallel.setToken("token1");
        this.sensorRepository.save(sensorParallel);

        Sensor barcelonetaParallel = new Sensor();
        barcelonetaParallel.setCity("Barcelona");
        barcelonetaParallel.setDistrict("Barceloneta");
        barcelonetaParallel.setToken("token2");
        this.sensorRepository.save(barcelonetaParallel);

        Sensor towerBridge = new Sensor();
        towerBridge.setCity("London");
        towerBridge.setDistrict("Tower Bridge");
        towerBridge.setToken("token3");
        this.sensorRepository.save(towerBridge);

        Sensor canaryWharf = new Sensor();
        canaryWharf.setCity("London");
        canaryWharf.setDistrict("Canary Wharf");
        canaryWharf.setToken("token4");
        this.sensorRepository.save(canaryWharf);

        Sensor piazzaLaRepubblica = new Sensor();
        piazzaLaRepubblica.setCity("Rome");
        piazzaLaRepubblica.setDistrict("Piazza La Repubblica");
        piazzaLaRepubblica.setToken("token5");
        this.sensorRepository.save(piazzaLaRepubblica);
    }

    private LoginResponseDto login() {
        String url = "http://localhost:" + port + "/api/login";
        LoginRequestDto loginRequest = new LoginRequestDto();
        loginRequest.setUsername("user");
        loginRequest.setPassword("password");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LoginRequestDto> entity = new HttpEntity<>(loginRequest, headers);
        ResponseEntity<LoginResponseDto> response = restTemplate.exchange(
                url, HttpMethod.POST, entity, LoginResponseDto.class
        );

        return response.getBody();
    }

    @Test
    void testGetSensors() {
        // Test to get the existing sensors
        LoginResponseDto loginResponseDto = this.login();
        String url = "http://localhost:" + port + "/api/sensor";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "  + loginResponseDto.getToken().getAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<PageResponse<SensorResponseDto>> response = restTemplate.exchange(
                url, HttpMethod.GET, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody()).isInstanceOf(PageResponse.class);
        Assertions.assertThat((long) response.getBody().getResult().size()).isEqualTo(5);
        List<SensorResponseDto> sensorResponseDtos = response.getBody().getResult();
        Assertions.assertThat(sensorResponseDtos.get(0).getToken()).isEqualTo("token1");
        Assertions.assertThat(sensorResponseDtos.get(1).getToken()).isEqualTo("token2");
        Assertions.assertThat(sensorResponseDtos.get(2).getToken()).isEqualTo("token3");
        Assertions.assertThat(sensorResponseDtos.get(3).getToken()).isEqualTo("token4");
        Assertions.assertThat(sensorResponseDtos.get(4).getToken()).isEqualTo("token5");

        // Test to get sensor by city and district
        url = "http://localhost:" + port + "/api/sensor?city=London&district=Tower+Bridge";
        response = restTemplate.exchange(
                url, HttpMethod.GET, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody()).isInstanceOf(PageResponse.class);
        Assertions.assertThat((long) response.getBody().getResult().size()).isEqualTo(1);
        sensorResponseDtos = response.getBody().getResult();
        Assertions.assertThat(sensorResponseDtos.get(0).getToken()).isEqualTo("token3");
    }

    @Test
    void testWithoutLogin() {
        // Test wrong request that is unauthorized (unauthenticated)
        String url = "http://localhost:" + port + "/api/sensor";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<PageResponse<SensorResponseDto>> response = restTemplate.exchange(
                url, HttpMethod.GET, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    void testGetSensor() {
        // Get the London, Tower Bridge sensor id by reading list api and test read by id api
        LoginResponseDto loginResponseDto = this.login();
        String url = "http://localhost:" + port + "/api/sensor?city=London&district=Tower+Bridge";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "  + loginResponseDto.getToken().getAccessToken());
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<PageResponse<SensorResponseDto>> response = restTemplate.exchange(
                url, HttpMethod.GET, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody()).isInstanceOf(PageResponse.class);
        Assertions.assertThat((long) response.getBody().getResult().size()).isEqualTo(1);
        List<SensorResponseDto> sensorResponseDtos = response.getBody().getResult();
        Assertions.assertThat(sensorResponseDtos.get(0).getToken()).isEqualTo("token3");

        Long sensorId = sensorResponseDtos.get(0).getId();
        url = "http://localhost:" + port + "/api/sensor/" + sensorId;
        ResponseEntity<SensorResponseDto> singleSensorResponse = restTemplate.exchange(
                url, HttpMethod.GET, entity, SensorResponseDto.class
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(singleSensorResponse).isNotNull();
        Assertions.assertThat(Objects.requireNonNull(singleSensorResponse.getBody()).getToken()).isEqualTo("token3");
    }

    @Test
    void testCreateSensor() {
        // Try testing the create sensor api
        LoginResponseDto loginResponseDto = this.login();
        String url = "http://localhost:" + port + "/api/sensor";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "  + loginResponseDto.getToken().getAccessToken());
        SensorCreateRequestDto sensorCreateRequestDto = new SensorCreateRequestDto();
        sensorCreateRequestDto.setCity("Liverpool");
        sensorCreateRequestDto.setDistrict("Anfield");
        HttpEntity<SensorCreateRequestDto> entity = new HttpEntity<>(sensorCreateRequestDto, headers);
        ResponseEntity<SensorResponseDto> response = restTemplate.exchange(
                url, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(Objects.requireNonNull(response.getBody()).getCity()).isEqualTo("Liverpool");
        Assertions.assertThat(Objects.requireNonNull(response.getBody()).getDistrict()).isEqualTo("Anfield");

        // After a success creation, try to create again with the same city and district
        response = restTemplate.exchange(
                url, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {}
        );
        // Will bring 422, because it is validated to not allow duplicate value between city and district
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
