package com.track.co2.sensor;


import com.track.co2.model.sensor.Sensor;
import com.track.co2.model.sensor.SensorTrack;
import com.track.co2.model.user.User;
import com.track.co2.repository.sensor.SensorRepository;
import com.track.co2.repository.sensor.SensorTrackRepository;
import com.track.co2.repository.user.UserRepository;
import com.track.co2.request.sensor.SensorTrackCreateRequestDto;
import com.track.co2.response.PageResponse;
import com.track.co2.response.sensor.SensorTrackResponseDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
public class SensorTrackControllerTest {
    @LocalServerPort
    private int port;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SensorRepository sensorRepository;

    @Autowired
    private SensorTrackRepository sensorTrackRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setup() {
        userRepository.deleteAll();
        User user = new User();
        user.setUsername("user");
        user.setPassword(passwordEncoder.encode("password"));
        userRepository.save(user);

        sensorTrackRepository.deleteAll();
        sensorRepository.deleteAll();

        // Add Parallel sensor with two tracks
        Sensor parallelSensor = new Sensor();
        parallelSensor.setCity("Barcelona");
        parallelSensor.setDistrict("Parallel");
        parallelSensor.setToken("token1");
        this.sensorRepository.save(parallelSensor);

        SensorTrack parallelSensorTrack1 = new SensorTrack();
        parallelSensorTrack1.setSensor(parallelSensor);
        parallelSensorTrack1.setTime(LocalDateTime.now().minusMinutes(5).withSecond(0));
        parallelSensorTrack1.setValue(2.0);
        this.sensorTrackRepository.save(parallelSensorTrack1);

        SensorTrack parallelSensorTrack2 = new SensorTrack();
        parallelSensorTrack2.setSensor(parallelSensor);
        parallelSensorTrack2.setTime(LocalDateTime.now().withSecond(0));
        parallelSensorTrack2.setValue(5.0);
        this.sensorTrackRepository.save(parallelSensorTrack2);

        // Add Barceloneta sensor with three tracks
        Sensor barcelonetaSensor = new Sensor();
        barcelonetaSensor.setCity("Barcelona");
        barcelonetaSensor.setDistrict("Barceloneta");
        barcelonetaSensor.setToken("token2");
        this.sensorRepository.save(barcelonetaSensor);

        SensorTrack barcelonetaSensorTrack1 = new SensorTrack();
        barcelonetaSensorTrack1.setSensor(barcelonetaSensor);
        barcelonetaSensorTrack1.setTime(LocalDateTime.now().minusMinutes(10).withSecond(0));
        barcelonetaSensorTrack1.setValue(1.0);
        this.sensorTrackRepository.save(barcelonetaSensorTrack1);

        SensorTrack barcelonetaSensorTrack2 = new SensorTrack();
        barcelonetaSensorTrack2.setSensor(barcelonetaSensor);
        barcelonetaSensorTrack2.setTime(LocalDateTime.now().minusMinutes(5).withSecond(0));
        barcelonetaSensorTrack2.setValue(3.0);
        this.sensorTrackRepository.save(barcelonetaSensorTrack2);

        SensorTrack barcelonetaSensorTrack3 = new SensorTrack();
        barcelonetaSensorTrack3.setSensor(barcelonetaSensor);
        barcelonetaSensorTrack3.setTime(LocalDateTime.now().withSecond(0));
        barcelonetaSensorTrack3.setValue(1.5);
        this.sensorTrackRepository.save(barcelonetaSensorTrack3);
    }

    @Test
    void testGetSensorTrack() {
        // Check tracks of the first sensor
        String url = "http://localhost:" + port + "/api/sensor/token1/track";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<PageResponse<SensorTrackResponseDto>> response = restTemplate.exchange(
                url, HttpMethod.GET, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody()).isInstanceOf(PageResponse.class);
        Assertions.assertThat((long) response.getBody().getResult().size()).isEqualTo(2);
        List<SensorTrackResponseDto> sensorTrackResponseDtos = response.getBody().getResult();
        Assertions.assertThat(sensorTrackResponseDtos.get(0).getValue()).isEqualTo(5.0);
        Assertions.assertThat(sensorTrackResponseDtos.get(1).getValue()).isEqualTo(2.0);

        // Check tracks of the second sensor
        url = "http://localhost:" + port + "/api/sensor/token2/track";
        response = restTemplate.exchange(
                url, HttpMethod.GET, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody()).isInstanceOf(PageResponse.class);
        Assertions.assertThat((long) response.getBody().getResult().size()).isEqualTo(3);
        sensorTrackResponseDtos = response.getBody().getResult();
        Assertions.assertThat(sensorTrackResponseDtos.get(0).getValue()).isEqualTo(1.5);
        Assertions.assertThat(sensorTrackResponseDtos.get(1).getValue()).isEqualTo(3.0);
        Assertions.assertThat(sensorTrackResponseDtos.get(2).getValue()).isEqualTo(1.0);

        // Check tracks of a not existing sensor
        url = "http://localhost:" + port + "/api/sensor/token_not_existing/track";
        response = restTemplate.exchange(
                url, HttpMethod.GET, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void testAddSensorTrack() {
        // Try testing the create track api
        String url = "http://localhost:" + port + "/api/sensor/token1/track";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        SensorTrackCreateRequestDto sensorTrackCreateRequestDto = new SensorTrackCreateRequestDto();
        sensorTrackCreateRequestDto.setValue(10.0);
        HttpEntity<SensorTrackCreateRequestDto> entity = new HttpEntity<>(sensorTrackCreateRequestDto, headers);
        ResponseEntity<SensorTrackResponseDto> response = restTemplate.exchange(
                url, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody()).isInstanceOf(SensorTrackResponseDto.class);
        SensorTrackResponseDto sensorTrackResponseDto = response.getBody();
        Assertions.assertThat(sensorTrackResponseDto.getValue()).isEqualTo(10.0);

        // Try testing the create track api with a not existing sensor
        url = "http://localhost:" + port + "/api/sensor/token_not_existing/track";
        response = restTemplate.exchange(
                url, HttpMethod.POST, entity, new ParameterizedTypeReference<>() {}
        );
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
}
