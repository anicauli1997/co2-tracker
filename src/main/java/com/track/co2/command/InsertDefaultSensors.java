package com.track.co2.command;

import com.track.co2.model.sensor.Sensor;
import com.track.co2.repository.sensor.SensorRepository;
import com.track.co2.service.sensor.SensorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
@Order(1)
public class InsertDefaultSensors implements CommandLineRunner {
    @Autowired
    private final SensorService sensorService;

    @Autowired
    private final SensorRepository sensorRepository;


    @Override
    public void run(String... args) throws Exception {
        List<Sensor> defaultSensors = this.getDefaultSensors();

        for (Sensor defaultSensor : defaultSensors) {
            Boolean exists = this.sensorRepository.existsByCityAndDistrict(
                defaultSensor.getCity(),
                defaultSensor.getDistrict()
            );
            if (exists) {
                continue;
            }

            this.sensorRepository.save(defaultSensor);
        }
    }

    private List<Sensor> getDefaultSensors() {
        List<Sensor> defaultSensors = new ArrayList<>();
        Sensor garciaSensor = this.sensorService.create("Barcelona", "Gràcia");
        defaultSensors.add(garciaSensor);

        Sensor eixampleSensor = this.sensorService.create("Barcelona", "Eixample");
        defaultSensors.add(eixampleSensor);

        Sensor wahringSensor = this.sensorService.create("Wien", "Währing");
        defaultSensors.add(wahringSensor);

        Sensor penzingSensor = this.sensorService.create("Wien", "Penzing");
        defaultSensors.add(penzingSensor);

        Sensor maxvorstadtSensor = this.sensorService.create("München", "Maxvorstadt");
        defaultSensors.add(maxvorstadtSensor);

        return defaultSensors;
    }
}
