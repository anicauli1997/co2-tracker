package com.track.co2.command;

import com.track.co2.model.sensor.Sensor;
import com.track.co2.model.sensor.SensorTrack;
import com.track.co2.repository.sensor.SensorRepository;
import com.track.co2.repository.sensor.SensorTrackRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
@RequiredArgsConstructor
@Order(2)
public class InsertDefaultSensorTracks implements CommandLineRunner {
    @Autowired
    private final SensorRepository sensorRepository;
    @Autowired
    private final SensorTrackRepository sensorTrackRepository;

    @Autowired
    private final Random random;

    @Override
    public void run(String... args) throws Exception {
        List<Sensor> sensors = this.sensorRepository.findAll();
        for (Sensor sensor : sensors) {
            if (this.sensorTrackRepository.countAllBySensor(sensor) > 0) {
                // This sensor already has tracks
                continue;
            }

            this.sensorTrackRepository.saveAll(this.generateRandomTracks(sensor));
        }
    }

    private List<SensorTrack> generateRandomTracks(Sensor sensor) {
        List<SensorTrack> sensorTracks = new ArrayList<>();
        // Generate random track number between 5 and 10 for a sensor
        int randomTrackNumber = random.nextInt(6) + 5;
        for (int i = randomTrackNumber; i > 0; i--) {
            SensorTrack sensorTrack = new SensorTrack();
            sensorTrack.setSensor(sensor);

            // Set a random value from 0 to 100 for the track value
            sensorTrack.setValue(100 * Math.random());
            int minutesFromNow = i * 5;
            LocalDateTime time = LocalDateTime.now().minusMinutes(minutesFromNow).withSecond(0);

            sensorTrack.setTime(time);
            sensorTracks.add(sensorTrack);
        }

        return sensorTracks;
    }
}
