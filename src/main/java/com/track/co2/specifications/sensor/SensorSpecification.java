package com.track.co2.specifications.sensor;

import com.track.co2.model.sensor.Sensor;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

public class SensorSpecification {
    public static Specification<Sensor> filter(String city, String district) {
        return (root, query, builder) -> {
            Predicate predicate = builder.conjunction();

            if (city != null && !city.isEmpty()) {
                predicate = builder.and(predicate, builder.equal(root.get("city"), city));
            }

            if (district != null && !district.isEmpty()) {
                predicate = builder.and(predicate, builder.equal(root.get("district"), district));
            }

            return predicate;
        };
    }
}
