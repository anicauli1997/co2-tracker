package com.track.co2.repository.sensor;

import com.track.co2.model.sensor.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SensorRepository extends JpaRepository<Sensor, Long>,
        JpaSpecificationExecutor<Sensor>,
        PagingAndSortingRepository<Sensor, Long> {
    Boolean existsByCityAndDistrict(String city, String district);
    Boolean existsByToken(String token);

    Optional<Sensor> findByToken(String token);
}
