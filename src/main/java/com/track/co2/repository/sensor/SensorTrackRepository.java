package com.track.co2.repository.sensor;

import com.track.co2.model.sensor.Sensor;
import com.track.co2.model.sensor.SensorTrack;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SensorTrackRepository extends JpaRepository<SensorTrack, Long> {
    Integer countAllBySensor(Sensor sensor);

    Page<SensorTrack> findAllBySensorOrderByTimeDesc(Sensor sensor, Pageable pageable);
}
