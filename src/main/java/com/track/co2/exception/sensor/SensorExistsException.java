package com.track.co2.exception.sensor;

public class SensorExistsException extends RuntimeException {
    String message;
    public SensorExistsException() {
        this.message = "Sensor already exists";
    }
}
