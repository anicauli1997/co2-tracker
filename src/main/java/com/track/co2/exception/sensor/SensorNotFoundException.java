package com.track.co2.exception.sensor;

import com.track.co2.exception.NotFoundException;

public class SensorNotFoundException extends NotFoundException {
    public SensorNotFoundException() {
        super("Sensor does not exists", "Sensor");
    }
}
