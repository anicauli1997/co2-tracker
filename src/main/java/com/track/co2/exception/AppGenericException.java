package com.track.co2.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class AppGenericException extends RuntimeException {
    private HttpStatus status;
    private String message;

    public AppGenericException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }
}
