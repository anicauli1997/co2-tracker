package com.track.co2.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class NotFoundException extends RuntimeException {
    private String message;
    private String entity;
    public NotFoundException(String message, String entity) {
        this.message = message;
        this.entity = entity;
    }
}
