package com.track.co2.request.sensor;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SensorCreateRequestDto {
    @NotNull
    private String city;

    @NotNull
    private String district;
}
