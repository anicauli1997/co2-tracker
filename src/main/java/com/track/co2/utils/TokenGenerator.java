package com.track.co2.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.SecureRandom;

@RequiredArgsConstructor
@Component
public class TokenGenerator {

    @Autowired
    private final SecureRandom secureRandom;

    public String generateToken(int length) {
        byte[] bytes = new byte[length / 2];
        this.secureRandom.nextBytes(bytes);
        return new BigInteger(1, bytes).toString(16);
    }
}
