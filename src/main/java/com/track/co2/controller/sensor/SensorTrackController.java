package com.track.co2.controller.sensor;

import com.track.co2.model.sensor.Sensor;
import com.track.co2.request.sensor.SensorTrackCreateRequestDto;
import com.track.co2.response.PageResponse;
import com.track.co2.response.sensor.SensorTrackResponseDto;
import com.track.co2.service.sensor.SensorService;
import com.track.co2.service.sensor.SensorTrackService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/sensor/{sensorToken}/track")
@RequiredArgsConstructor
public class SensorTrackController {

    @Autowired
    private final SensorService sensorService;
    @Autowired
    private final SensorTrackService sensorTrackService;

    @GetMapping
    public ResponseEntity<PageResponse<SensorTrackResponseDto>> getSensorTracks(
        @PathVariable("sensorToken") String sensorToken,
        @RequestParam(defaultValue = "1") int pageNo,
        @RequestParam(defaultValue = "10") int pageSize
    ) {
        Sensor sensor = this.sensorService.findByToken(sensorToken);
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return ResponseEntity.ok(this.sensorTrackService.getSensorTracks(pageable, sensor));
    }

    @PostMapping
    public ResponseEntity<SensorTrackResponseDto> saveSensorTrack(
        @PathVariable("sensorToken") String sensorToken,
        @RequestBody @Valid SensorTrackCreateRequestDto sensorTrackCreateRequestDto
    ) {
        Sensor sensor = this.sensorService.findByToken(sensorToken);
        return ResponseEntity.ok(this.sensorTrackService.saveSensorTrack(
            sensor,
            sensorTrackCreateRequestDto.getValue()
        ));
    }
}
