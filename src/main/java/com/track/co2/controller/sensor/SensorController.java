package com.track.co2.controller.sensor;

import com.track.co2.exception.ValidationException;
import com.track.co2.exception.sensor.SensorExistsException;
import com.track.co2.request.sensor.SensorCreateRequestDto;
import com.track.co2.response.PageResponse;
import com.track.co2.response.sensor.SensorResponseDto;
import com.track.co2.service.sensor.SensorService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/api/sensor")
@SecurityRequirement(name = "bearerAuth")
public class SensorController {

    @Autowired
    private final SensorService sensorService;

    @GetMapping
    public ResponseEntity<PageResponse<SensorResponseDto>> getSensors(
        @RequestParam(defaultValue = "1") int pageNo,
        @RequestParam(defaultValue = "10") int pageSize,
        @RequestParam(defaultValue = "") String city,
        @RequestParam(defaultValue = "") String district
    ) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return ResponseEntity.ok(this.sensorService.getSensors(pageable, city, district));
    }

    @GetMapping("/{sensorId}")
    public ResponseEntity<SensorResponseDto> getSensor(
        @PathVariable("sensorId") Long sensorId
    ) {
        return ResponseEntity.ok(this.sensorService.getSensor(sensorId));
    }

    @PostMapping
    public ResponseEntity<SensorResponseDto> createSensor(
        @RequestBody @Valid SensorCreateRequestDto sensorCreateRequestDto
    ) {
        try {
            return ResponseEntity.ok(this.sensorService.create(sensorCreateRequestDto));
        } catch (SensorExistsException sensorExistsException) {
            Map<String, String> errors = new HashMap<>();
            String message = "The sensor with the provided city and district already exists";
            errors.put("city", message);
            errors.put("district", message);
            throw new ValidationException(errors);
        }
    }
}
