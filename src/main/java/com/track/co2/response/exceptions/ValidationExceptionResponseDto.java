package com.track.co2.response.exceptions;

import com.track.co2.exception.ValidationException;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class ValidationExceptionResponseDto {
    private final Object errors;
    private final String message = "The given data is invalid";
    public ValidationExceptionResponseDto(BindException bindException) {
        Map<String, String> validationErrors = new HashMap<>();
        bindException.getBindingResult().getAllErrors().forEach((error) -> {
            FieldError e = ((FieldError) error);
            String fieldName = e.getField();
            String fieldMessage;
            fieldMessage = e.getDefaultMessage();
            validationErrors.put(fieldName, fieldMessage);
        });

        this.errors = validationErrors;
    }

    public ValidationExceptionResponseDto(Object errors) {
        this.errors = errors;
    }

    public ValidationExceptionResponseDto(ValidationException validationException) {
        this.errors = validationException.getErrors();
    }
}
