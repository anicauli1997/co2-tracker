package com.track.co2.response.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NotFoundExceptionResponseDto {
    private String message;
    private String entity;
}
