package com.track.co2.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageResponse<Entity> {
    private int totalPages;
    private int currentPage;
    private boolean hasNextPage;
    private boolean hasPreviousPage;
    private List<Entity> result;
    private int pageRecords;
    private Long totalRecords;

    public PageResponse(Page<?> page, List<Entity> items) {
        this.totalPages = page.getTotalPages();
        this.currentPage = page.getPageable().getPageNumber() + 1;
        this.hasNextPage = page.hasNext();
        this.hasPreviousPage = page.hasPrevious();
        this.result = items;
        this.pageRecords = this.result.size();
        this.totalRecords = page.getTotalElements();
    }
}
