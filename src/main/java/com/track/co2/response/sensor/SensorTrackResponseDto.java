package com.track.co2.response.sensor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SensorTrackResponseDto {
    private Long id;
    private Double value;
    private LocalDateTime time;
}
