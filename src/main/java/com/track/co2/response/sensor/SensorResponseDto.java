package com.track.co2.response.sensor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SensorResponseDto {
    private Long id;
    private String token;
    private String city;
    private String district;
}
