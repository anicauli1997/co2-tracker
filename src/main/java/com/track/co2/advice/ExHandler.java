package com.track.co2.advice;

import com.track.co2.exception.AppGenericException;
import com.track.co2.exception.NotFoundException;
import com.track.co2.exception.ValidationException;
import com.track.co2.response.exceptions.AppGenericExceptionResponseDto;
import com.track.co2.response.exceptions.NotFoundExceptionResponseDto;
import com.track.co2.response.exceptions.ValidationExceptionResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(NotFoundException notFoundException) {
        NotFoundExceptionResponseDto notFoundExceptionResponseDto = new NotFoundExceptionResponseDto();
        notFoundExceptionResponseDto.setMessage(notFoundException.getMessage());
        notFoundExceptionResponseDto.setEntity(notFoundException.getEntity());

        return new ResponseEntity<>(notFoundExceptionResponseDto, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AppGenericException.class)
    protected ResponseEntity<Object> handleAppGenericException(AppGenericException ex) {
        return new ResponseEntity<>(new AppGenericExceptionResponseDto(ex.getMessage()), ex.getStatus());
    }

    @ExceptionHandler(ValidationException.class)
    protected ResponseEntity<Object> handleValidationException(ValidationException ex) {
        return new ResponseEntity<>(new ValidationExceptionResponseDto(ex), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(BindException.class)
    protected ResponseEntity<Object> handleBindException(BindException ex) {
        return new ResponseEntity<>(new ValidationExceptionResponseDto(ex), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(new ValidationExceptionResponseDto(ex), HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
