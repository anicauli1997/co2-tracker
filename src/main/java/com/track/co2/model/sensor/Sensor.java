package com.track.co2.model.sensor;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(
    name = "sensors",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"city", "district"}),
        @UniqueConstraint(columnNames = {"token"})
    }
)
@Getter
@Setter
public class Sensor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 30, nullable = false)
    private String token;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String district;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @OneToMany(mappedBy = "sensor", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SensorTrack> tracks = new HashSet<>();
}
