package com.track.co2.service.sensor.impl;

import com.track.co2.config.sensor.SensorPropertiesConfig;
import com.track.co2.exception.sensor.SensorExistsException;
import com.track.co2.exception.sensor.SensorNotFoundException;
import com.track.co2.model.sensor.Sensor;
import com.track.co2.repository.sensor.SensorRepository;
import com.track.co2.request.sensor.SensorCreateRequestDto;
import com.track.co2.response.PageResponse;
import com.track.co2.response.sensor.SensorResponseDto;
import com.track.co2.service.sensor.SensorService;
import com.track.co2.specifications.sensor.SensorSpecification;
import com.track.co2.utils.TokenGenerator;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class SensorServiceImpl implements SensorService {
    @Autowired
    private final TokenGenerator tokenGenerator;

    @Autowired
    private final SensorRepository sensorRepository;

    @Autowired
    private final SensorPropertiesConfig sensorPropertiesConfig;

    @Autowired
    private final ModelMapper mapper;

    @Override
    public Sensor create(String city, String district, Boolean commit) {
        Sensor sensor = new Sensor();
        sensor.setCity(city);
        sensor.setDistrict(district);
        String token;
        do {
            token = this.tokenGenerator.generateToken(this.sensorPropertiesConfig.getTokenLength());
        } while (this.sensorRepository.existsByToken(token));
        sensor.setToken(token);
        if (commit) {
            this.sensorRepository.save(sensor);
        }
        return sensor;
    }

    @Override
    public Sensor create(String city, String district) {
        return this.create(city, district, false);
    }

    @Override
    public SensorResponseDto create(SensorCreateRequestDto sensorCreateRequestDto) {
        Boolean sensorExists = this.sensorRepository.existsByCityAndDistrict(
            sensorCreateRequestDto.getCity(),
            sensorCreateRequestDto.getDistrict()
        );

        if (sensorExists) {
            throw new SensorExistsException();
        }

        Sensor sensor = this.create(
            sensorCreateRequestDto.getCity(),
            sensorCreateRequestDto.getDistrict(),
            true
        );

        return this.mapToSensorResponseDto(sensor);
    }

    @Override
    public Sensor findByToken(String sensorToken) {
        Optional<Sensor> sensorOptional = this.sensorRepository.findByToken(sensorToken);
        if (sensorOptional.isEmpty()) {
            throw new SensorNotFoundException();
        }

        return sensorOptional.get();
    }

    @Override
    public PageResponse<SensorResponseDto> getSensors(Pageable pageable, String city, String district) {
        Page<Sensor> sensorPage = this.sensorRepository.findAll(
            SensorSpecification.filter(city, district),
            pageable
        );
        List<SensorResponseDto> sensorResponseDtos = sensorPage
                .getContent().stream().map(this::mapToSensorResponseDto).toList();

        return new PageResponse<>(sensorPage, sensorResponseDtos);
    }

    @Override
    public SensorResponseDto getSensor(Long sensorId) {
        Optional<Sensor> optionalSensor = this.sensorRepository.findById(sensorId);
        if (optionalSensor.isEmpty()) {
            throw new SensorNotFoundException();
        }

        return this.mapToSensorResponseDto(optionalSensor.get());
    }

    public SensorResponseDto mapToSensorResponseDto(Sensor sensor) {
        return this.mapper.map(sensor, SensorResponseDto.class);
    }
}
