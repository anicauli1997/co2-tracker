package com.track.co2.service.sensor;

import com.track.co2.model.sensor.Sensor;
import com.track.co2.request.sensor.SensorCreateRequestDto;
import com.track.co2.response.PageResponse;
import com.track.co2.response.sensor.SensorResponseDto;
import org.springframework.data.domain.Pageable;

public interface SensorService {
    Sensor create(String city, String district, Boolean commit);

    Sensor create(String city, String district);

    Sensor findByToken(String sensorToken);

    PageResponse<SensorResponseDto> getSensors(Pageable pageable, String city, String district);
    SensorResponseDto getSensor(Long sensorId);

    SensorResponseDto mapToSensorResponseDto(Sensor sensor);

    SensorResponseDto create(SensorCreateRequestDto sensorCreateRequestDto);
}
