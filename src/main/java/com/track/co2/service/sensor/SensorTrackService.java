package com.track.co2.service.sensor;

import com.track.co2.model.sensor.Sensor;
import com.track.co2.response.PageResponse;
import com.track.co2.response.sensor.SensorTrackResponseDto;
import org.springframework.data.domain.Pageable;

public interface SensorTrackService {
    PageResponse<SensorTrackResponseDto> getSensorTracks(Pageable pageable, Sensor sensor);

    SensorTrackResponseDto saveSensorTrack(Sensor sensor, Double value);
}
