package com.track.co2.service.sensor.impl;

import com.track.co2.model.sensor.Sensor;
import com.track.co2.model.sensor.SensorTrack;
import com.track.co2.repository.sensor.SensorTrackRepository;
import com.track.co2.response.PageResponse;
import com.track.co2.response.sensor.SensorTrackResponseDto;
import com.track.co2.service.sensor.SensorTrackService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@Service
public class SensorTrackServiceImpl implements SensorTrackService {

    @Autowired
    private final SensorTrackRepository sensorTrackRepository;

    @Autowired
    private final ModelMapper mapper;

    @Override
    public PageResponse<SensorTrackResponseDto> getSensorTracks(Pageable pageable, Sensor sensor) {
        Page<SensorTrack> sensorTrackServicePage = this.sensorTrackRepository
                .findAllBySensorOrderByTimeDesc(sensor, pageable);
        List<SensorTrackResponseDto> sensorTrackResponseDtos = sensorTrackServicePage
                .getContent().stream().map(this::mapToSensorTrackResponseDto).toList();

        return new PageResponse<>(sensorTrackServicePage, sensorTrackResponseDtos);
    }

    public SensorTrackResponseDto saveSensorTrack(Sensor sensor, Double value) {
        SensorTrack sensorTrack = new SensorTrack();
        sensorTrack.setSensor(sensor);
        sensorTrack.setValue(value);
        sensorTrack.setTime(LocalDateTime.now());
        this.sensorTrackRepository.save(sensorTrack);
        return this.mapToSensorTrackResponseDto(sensorTrack);
    }

    private SensorTrackResponseDto mapToSensorTrackResponseDto(SensorTrack sensorTrack) {
        return this.mapper.map(sensorTrack, SensorTrackResponseDto.class);
    }
}
