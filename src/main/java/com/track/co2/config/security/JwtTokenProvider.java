package com.track.co2.config.security;

import com.track.co2.dto.user.TokenDto;
import com.track.co2.exception.AppGenericException;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenProvider {
    @Value("${security.jwt-secret}")
    private String jwtSecret;

    @Value("${security.jwt-expiration-milliseconds}")
    private int jwtExpirationMilliseconds;

    public TokenDto generateToken(Authentication authentication) {
        String username = authentication.getName();
        Date currentDate = new Date();
        Date expiringDate = new Date(currentDate.getTime() + this.jwtExpirationMilliseconds);
        Date refreshTokenExp = new Date((currentDate.getTime() - 2000) + 2L * this.jwtExpirationMilliseconds);

        String accessToken = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(expiringDate)
                .signWith(SignatureAlgorithm.HS512, this.jwtSecret)
                .compact();

        String refreshToken = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(refreshTokenExp)
                .signWith(SignatureAlgorithm.HS512, this.jwtSecret)
                .compact();

        return new TokenDto(accessToken, expiringDate, refreshToken, refreshTokenExp);
    }

    public String getUsernameFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(this.jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }

    public Boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(this.jwtSecret).parse(token);
            return true;
        } catch (SignatureException signatureException) {
            throw new AppGenericException(HttpStatus.BAD_REQUEST, "Invalid JWT signature");
        } catch (MalformedJwtException malformedJwtException) {
            throw new AppGenericException(HttpStatus.BAD_REQUEST, "Invalid JWT token");
        } catch (ExpiredJwtException expiredJwtException) {
            throw new AppGenericException(HttpStatus.BAD_REQUEST, "Expired JWT token");
        } catch (UnsupportedJwtException unsupportedJwtException) {
            throw new AppGenericException(HttpStatus.BAD_REQUEST, "Unsupported JWT token");
        } catch (IllegalArgumentException illegalArgumentException) {
            throw new AppGenericException(HttpStatus.BAD_REQUEST, "JWT claims string is empty");
        }
    }
}
