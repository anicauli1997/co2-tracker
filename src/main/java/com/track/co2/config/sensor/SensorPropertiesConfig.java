package com.track.co2.config.sensor;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "sensor")
@Getter
@Setter
public class SensorPropertiesConfig {
    private int tokenLength;
}
